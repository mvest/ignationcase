// ---------------- Response Functions --------------------------------------------------

'use strict';

//Close
module.exports.close = function(sessionAttributes, fulfillmentState, message) {
  return {
    sessionAttributes,
    dialogAction: {
      type: 'Close',
      fulfillmentState,
      message,
    },
  };
}

//Delegate
module.exports.delegate = function(sessionAttributes, slots) {
  console.log('sent delegate reply');
  return {
    sessionAttributes,
    dialogAction: {
      type: 'Delegate',
      slots,
    },
  };
}

//confrimIntent
module.exports.confirmIntent = function(sessionAttributes, intentName, slots, message) {
  console.log('sent confrimIntent reply');

  return {
    sessionAttributes,
    dialogAction: {
      type: 'ConfirmIntent',
      intentName,
      slots,
      message
    },
  };
}


//ElicitSlot
module.exports.elicitSlot = function(sessionAttributes, intentName, slots, slotToElicit, message) {

  console.log('Elicit slot called');

  return {
    sessionAttributes,
    dialogAction: {
      type: 'ElicitSlot',
      intentName,
      slots,
      slotToElicit,
      message,
    }
  };
}

'use strict';
const lexResponses = require('./lexResponses.js');

//intent moduels
const greeting = require('./greeting.js');
const weatherForecast = require('./weatherForecast.js');
const JuiceOrder = require('./juiceOrder.js');

// --------------- Main handler -----------------------

// Route the incoming request based on intent.
exports.handler = (event, context, callback) => {
  try {
    console.log(`event.bot.name=${event.bot.name}`);

    dispatch(event, (response) => callback(null, response));
  } catch (err) {
    callback(err);
  }
};

// --------------- Intents -----------------------

/**
 * Called when the user specifies an intent for this skill.
 */
function dispatch(intentRequest, callback) {
  console.log(`dispatch userId=${intentRequest.userId}, intentName=${intentRequest.currentIntent.name}`);

  const intentName = intentRequest.currentIntent.name;

  // Dispatch intent handlers

  if (intentName === 'Greeting') {
    return greeting(intentRequest, callback);
  }

  if (intentName === 'Weather') {
    return weatherForecast(intentRequest, callback);
  }

  if (intentName === 'JuiceOrder') {
    return JuiceOrder(intentRequest, callback);
  }

  throw new Error(`Intent with name ${intentName} not supported`);
}

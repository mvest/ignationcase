'use strict'

const https = require('https');
const lexResponses = require('./lexResponses.js');

module.exports = function(intentRequest, callback) {
  const locationQuery = intentRequest.currentIntent.slots.location;
  const time = intentRequest.currentIntent.slots.time;
  const source = intentRequest.invocationSource;
  const slots = intentRequest.currentIntent.slots;


  if (source === 'DialogCodeHook') {

    if (intentRequest.currentIntent.confirmationStatus === 'Denied') {


    }

    if (intentRequest.currentIntent.confirmationStatus === 'None') {

      validateLocation(locationQuery) //returns a promies
        .then(locationResult => {

          if (locationResult.type === 'no results') {

            slots[`location`] = null;

            callback(lexResponses.elicitSlot(
              intentRequest.sessionAttributes,
              intentRequest.currentIntent.name,
              slots,
              'location', {
                contentType: 'PlainText',
                content: `I could not find any location similar to ${locationQuery}, give me an other location`
              }
            ));
            return;


          }

          if (locationResult.type === 'close match') {

            let sessionAttributes = {
              latlng: locationResult.latlng,
              location: locationResult.closestMatchName
            }

            callback(lexResponses.confirmIntent(sessionAttributes,
              intentRequest.currentIntent.name,
              intentRequest.currentIntent.slots, {
                contentType: "PlainText",
                content: `Did you mean ${locationResult.closestMatchDesc}?`
              }
            ));
            return;

          }

          if (locationResult.type === 'success') {

            getWeatherData(locationQuery, locationResult.latlng)
              .then(result => {
                callback(lexResponses.close(intentRequest.sessionAttributes, 'Fulfilled', {
                  contentType: 'PlainText',
                  content: result
                }));
              })
              .catch(err => {
                // if weatherdata failed. catch it here
                console.log(err);
                callback(lexResponses.close(intentRequest.sessionAttributes, 'Failed', {
                  contentType: 'PlainText',
                  content: 'Something went wrong with the weather search. please try again later'
                }));
              });

          }

        })
        .catch(err => {
          //if validateLocation fails
          console.log(err);
          callback(lexResponses.close(intentRequest.sessionAttributes, 'Failed', {
            contentType: 'PlainText',
            content: 'Something went wrong with the location search. please try again later'
          }));

        });
    }
    //END confirmationStatus None

    //if the user has confirmed the close match
    if (intentRequest.currentIntent.confirmationStatus === 'Confirmed') {

      getWeatherData(intentRequest.sessionAttributes.location, intentRequest.sessionAttributes.latlng)
        .then(result => {
          callback(lexResponses.close(intentRequest.sessionAttributes, 'Fulfilled', {
            contentType: 'PlainText',
            content: result
          }));
        })
        .catch(err => {
          // if weatherdata failed. catch it here
          console.log(err);
          callback(lexResponses.close(intentRequest.sessionAttributes, 'Failed', {
            contentType: 'PlainText',
            content: 'Something went wrong with the search. please try again later'
          }));
        });


    }

    //if user denied the close match suggestion
    if (intentRequest.currentIntent.confirmationStatus === 'Denied') {

      slots[`location`] = null;

      callback(lexResponses.elicitSlot(
        intentRequest.sessionAttributes,
        intentRequest.currentIntent.name,
        slots,
        'location', {
          contentType: 'PlainText',
          content: `Okay make it more specific or give me an other location`
        }
      ));
      return;


    }

  }

}

function validateLocation(loc) {
  const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${loc}.json?access_token=pk.eyJ1IjoibWFnOTAiLCJhIjoiY2plN2F2a3NpMGF2eDJxbnQ5czQwbG8wZiJ9.vpHgLqUfN0__wxfXugYwvA`;
  return new Promise((resolve, reject) => {

    https.get(url, res => {
      res.setEncoding("utf8");
      let body = "";

      res.on("data", data => {
        body += data;
      });

      res.on('error', err => {
        reject(err);
      });

      res.on("end", () => {
        body = JSON.parse(body);
        //console.log(JSON.stringify(body));

        //if no result is returned
        if (body.features.length < 1) {
          let result = {
            type: "no results"
          }
          resolve(result);
        } else {

          //if a close match is returned
          if (body.features[0].text.toLowerCase() !== loc.toLowerCase()) {
            let result = {
              type: "close match",
              closestMatchDesc: body.features[0].place_name,
              closestMatchName: body.features[0].text,
              latlng: String(body.features[0].geometry.coordinates)
            }
            resolve(result);
          } else { // if an exact match is returned.
            let result = {
              type: 'success',
              name: body.features[0].text,
              latlng: String(body.features[0].geometry.coordinates)
            }
            resolve(result);
          }
        }

      });
    });
  });
}

function getWeatherData(locationQuery, latlng) {

  latlng = latlng.split(',');
  let url = "https://api.darksky.net/forecast/19f367e7fa4444024942fd6d4da5548d/" + latlng[1] + "," + latlng[0];
  //exclude some data
  url += '?exclude=minutely,hourly,daily,alerts,flags';
  //setup units
  url += '&units=si';

  return new Promise((resolve, reject) => {

    https.get(url, res => {
      res.setEncoding("utf8");
      let body = "";

      res.on("data", data => {
        body += data;
      });

      res.on('error', err => {
        reject(err);
      });


      res.on("end", () => {
        body = JSON.parse(body);

        let message = `The weather in ${locationQuery} is ${body.currently.summary}. The temperature is ${body.currently.temperature}°C but feels like ${body.currently.apparentTemperature}°C. WindSpeed is ${body.currently.windSpeed} m/s with gusts up to ${body.currently.windGust} m/s`;

        resolve(message);
      });
    });

  });
}

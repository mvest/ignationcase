'use strict';

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient({
  region: 'eu-west-1'
});

const lexResponses = require('./lexResponses.js');

module.exports = function(intentRequest, callback) {

  const source = intentRequest.invocationSource;
  if (source === 'FulfillmentCodeHook') {

    mostPopular()
      .then(response => {
        callback(lexResponses.close(intentRequest.sessionAttributes, 'Fulfilled', {
          contentType: 'PlainText',
          content: `Hi, how can i help you today?. You can ask me about the weahter or order some virtual juice from the bar. ${response}`
        }));
      })
      .catch(err => {
        callback(err);
      })
  }

}

//TODO improve logic to handle when 2 drinks have the same top qty
function mostPopular() {
  return new Promise((resolve, reject) => {

    let query = {
      TableName: 'Orders',
      Limit: 100
    }

    docClient.scan(query, function(err, data) {
      if (err) {
        reject(err)
      } else {
        //Sort the biggest qty first in the array
        data.Items.sort(function(a, b) {
          return b.qty - a.qty
        });

        const matchingQtys = new Set(data.Items.map(o => o.qty));
        if (matchingQtys.size === 1) {
          resolve('All our juices are equally popular right now, so your order would be a game changer')
        } else {
          resolve('Our most pouplare drink is: ' + data.Items[0].JuiceType);
        }
      }

    });

  });

}

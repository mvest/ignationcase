'use strict';

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient({
  region: 'eu-west-1'
});

const lexResponses = require('./lexResponses.js');

module.exports = function(intentRequest, callback) {
  let juiceType = intentRequest.currentIntent.slots.type;
  let juiceSize = intentRequest.currentIntent.slots.size;

  const source = intentRequest.invocationSource;
  if (source === 'DialogCodeHook') {

    console.log(JSON.stringify(intentRequest));

    const slots = intentRequest.currentIntent.slots;
    const validationResult = validateJuiceOrder(juiceType, juiceSize);

    //IF any slot is invalid. send elicitSlot reply
    if (!validationResult.isValid) {
      slots[`${validationResult.violatedSlot}`] = null;
      callback(lexResponses.elicitSlot(intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots, validationResult.violatedSlot, validationResult.message));
      return;
    }

    //IF validation is fine. and confirmation is none. send confirmIntent reply
    if (intentRequest.currentIntent.confirmationStatus === 'None') {
      callback(lexResponses.confirmIntent(
        intentRequest.sessionAttributes,
        intentRequest.currentIntent.name,
        intentRequest.currentIntent.slots, {
          contentType: "PlainText",
          content: `are you sure you want to order a ${juiceSize} ${juiceType}`
        }
      ));
      return;
    }

    //If confirmation is denied. Close with message
    if (intentRequest.currentIntent.confirmationStatus === 'Denied') {
      callback(lexResponses.close(intentRequest.sessionAttributes, 'Failed', {
        contentType: 'PlainText',
        content: 'Okay, thats fine'
      }));
    }


    //If confirmation is confirmed. Place order.
    placeOrder(juiceType)
      .then(result => {
        callback(lexResponses.close(intentRequest.sessionAttributes, 'Fulfilled', {
          contentType: 'PlainText',
          content: `Alright here is a fresh viritual ${juiceSize} ${juiceType} juice!! 🍹 Thank you for you order`
        }));
      }).catch(err => {
        if (err) {
          callback(lexResponses.close(intentRequest.sessionAttributes, 'Failed', {
            contentType: 'PlainText',
            content: `Something went wrong with our ordering system. Please try again later`
          }));
        }
      });

  }




}

function validateJuiceOrder(juiceType, juiceSize) {

  const juiceTypeList = ['raspberry', 'pineapple', 'strawberry'];
  const juiceSizeList = ['small', 'regular', 'large'];


  //Check if juice type is avalible
  if (juiceType && juiceTypeList.indexOf(juiceType.toLowerCase()) === -1) {
    return buildValidationResult(false, 'type', `We do not have ${juiceType}, We have Raspberry, Pineapple and Strawberry :)`);
  } else if (juiceType === null) {
    return buildValidationResult(false, 'type', `What kind of Juice do you want? We have the following: Raspberry, Pineapple and Strawberry`);
  }



  //Check if juice Size is avalible
  if (juiceSize && juiceSizeList.indexOf(juiceSize.toLowerCase()) === -1) {
    return buildValidationResult(false, 'size', `We do not have ${juiceSize}. You can choose small, regular or large`);
  } else if (juiceSize === null) {
    return buildValidationResult(false, 'size', `What size do you want?`);
  }

  //slots are is valid
  return buildValidationResult(true, null, null);
}

function buildValidationResult(isValid, violatedSlot, messageContent) {
  if (messageContent == null) {
    return {
      isValid,
      violatedSlot,
    };
  }
  return {
    isValid,
    violatedSlot,
    message: {
      contentType: 'PlainText',
      content: messageContent
    },
  };
}


function placeOrder(juiceType) {
  return new Promise((resolve, reject) => {

    //make first letter upperCase
    juiceType = juiceType.toLowerCase().substr(0, 1).toUpperCase() + juiceType.substr(1)

    let query = {
      TableName: 'Orders',
      Key: {
        "JuiceType": juiceType
      }
    }
    //get current order count for juiceType
    docClient.get(query, function(err, data) {
      if (err) {
        reject(err)
      } else {

        var params = {
          Item: {
            JuiceType: data.Item.JuiceType,
            qty: (data.Item.qty += 1)
          },
          TableName: 'Orders'
        }

        //Update JuiceType DB count
        docClient.put(params, function(err, data) {
          if (err) {
            reject(err)
          } else {
            resolve('success')
          }

        });

      }
    });

  });
}
